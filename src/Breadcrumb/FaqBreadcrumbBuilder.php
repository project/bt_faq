<?php

namespace Drupal\bt_faq\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;

/**
 * Class FaqBreadcrumbBuilder.
 *
 * @package Drupal\bt_faq\Breadcrumb
 */
class FaqBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  /**
   * The site name.
   *
   * @var string
   */
  protected $siteName;

  /**
   * The routes that will change their breadcrumbs.
   *
   * @var array
   */
  private $routes = array(
    'node.add',
    'page_manager.page_view_app_website_faq_app_website_faq-panels_variant-0',
    'entity.node.edit_form',
  );

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->siteName = $configFactory->get('system.site')->get('name');
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $routeMatch) {
    $match = $this->routes;

    if (in_array($routeMatch->getRouteName(), $match)) {
      if ($routeMatch->getRouteName() == 'node.add') {
        if ($routeMatch->getParameters()->get('node_type')->get('type') == 'faq') {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }
      elseif ($routeMatch->getRouteName() == 'entity.node.edit_form') {
        if ($routeMatch->getParameters()->get('node')->bundle() == 'faq') {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $routeMatch) {
    $route = $routeMatch->getRouteName();
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(["url"]);
    $breadcrumb->addLink(Link::createFromRoute($this->siteName, 'page_manager.page_view_app_app-panels_variant-0'));
    $breadcrumb->addLink(Link::createFromRoute('Website', 'page_manager.page_view_app_website_app_website-panels_variant-0'));

    if ($route == 'entity.node.edit_form' || $route == 'node.add') {
      $breadcrumb->addLink(Link::createFromRoute('Frequent Answer Question', 'page_manager.page_view_app_website_faq_app_website_faq-panels_variant-0'));
    }

    return $breadcrumb;
  }

}
