<?php

namespace Drupal\bt_faq\Config;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\ConfigFactory;

/**
 * Example configuration override.
 */
class ConfigFaqOverride implements ConfigFactoryOverrideInterface {

  private $createContent;
  private $deleteContent;
  private $deleteOwnContent;
  private $editContent;
  private $viewsAdminContent;
  private $viewsFullAdminContent;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->createContent = $configFactory->get('user.role.bt_create_content');
    $this->deleteContent = $configFactory->get('user.role.bt_delete_content');
    $this->deleteOwnContent = $configFactory->get('user.role.bt_delete_own_content');
    $this->editContent = $configFactory->get('user.role.bt_edit_publish_content');
    $this->viewsAdminContent = $configFactory->get('views.view.bt_admin_content');
    $this->viewsAdminContent = $configFactory->get('views.view.bt_full_admin_content');
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    // TODO: Move spanish configurations
    $overrides = array();
    if (in_array('node.type.faq', $names)) {
      $overrides['node.type.faq']['dependencies']['module'] = array('menu_ui', 'workbench_moderation');
      //$overrides['node.type.faq']['help'] = 'El título de la pregunta será utilizado en el índice de su página de preguntas frecuentes.';
      $overrides['node.type.faq']['third_party_settings']['menu_ui'] = array(
        'available_menus' => array(),
        'parent' => '',
      );
      $overrides['node.type.faq']['third_party_settings']['workbench_moderation']['enabled'] = TRUE;
      $overrides['node.type.faq']['third_party_settings']['workbench_moderation']['allowed_moderation_states'] = [
        'published',
        'archived',
        'draft',
        'needs_review',
      ];
      $overrides['node.type.faq']['third_party_settings']['workbench_moderation']['default_moderation_state'] = 'draft';
      //$overrides['node.type.faq']['name'] = 'Respuesta a una pregunta frecuente';
      //$overrides['node.type.faq']['description'] = 'Una pregunta frecuente y su respuesta.';
      $overrides['node.type.faq']['preview_mode'] = 0;
      $overrides['node.type.faq']['display_submitted'] = FALSE;
    }
    if (in_array('field.field.node.faq.body', $names)) {
      //$overrides['field.field.node.faq.body']['description'] = 'Escriba aquí la respuesta a la pregunta.';
      $overrides['field.field.node.faq.body']['required'] = TRUE;
      $overrides['field.field.node.faq.body']['settings']['allowed_formats']['bt_simple_redaction'] = 'bt_simple_redaction';
      $overrides['field.field.node.faq.body']['settings']['allowed_formats']['bt_full_redaction'] = 0;
      $overrides['field.field.node.faq.body']['settings']['allowed_formats']['plain_text'] = 0;
      $overrides['field.field.node.faq.body']['settings']['display_summary'] = FALSE;
      //$overrides['field.field.node.faq.body']['label'] = 'Respuesta';
    }
    if (in_array('field.field.node.faq.field_detailed_question', $names)) {
      //$overrides['field.field.node.faq.field_detailed_question']['label'] = 'Detalle de la pregunta';
      //$overrides['field.field.node.faq.field_detailed_question']['description'] = 'Describa su pregunta lo más detallada posible.';
      $overrides['field.field.node.faq.field_detailed_question']['required'] = TRUE;
      $overrides['field.field.node.faq.field_detailed_question']['settings']['allowed_formats']['bt_simple_redaction'] = 'bt_simple_redaction';
      $overrides['field.field.node.faq.field_detailed_question']['settings']['allowed_formats']['bt_full_redaction'] = 0;
      $overrides['field.field.node.faq.field_detailed_question']['settings']['allowed_formats']['plain_text'] = 0;
      $overrides['field.field.node.faq.field_detailed_question']['settings']['display_summary'] = FALSE;
    }

    // Add article permissions to bt_create_content role.
    if (in_array('user.role.bt_create_content', $names)) {
      $faq_permissions = [
        'create faq content',
        'edit own faq content',
        'revert faq revisions',
        'view faq revisions',
      ];
      $content_role = $this->createContent;
      $permissions = array_merge($content_role->get('permissions'), $faq_permissions);
      $overrides['user.role.bt_create_content']['permissions'] = $permissions;
    }
    // Add article permissions to bt_delete_content role.
    if (in_array('user.role.bt_delete_content', $names)) {
      $faq_permissions = [
        'delete any faq content',
        'delete faq revisions',
      ];
      $content_role = $this->deleteContent;
      $permissions = array_merge($content_role->get('permissions'), $faq_permissions);
      $overrides['user.role.bt_delete_content']['permissions'] = $permissions;
    }
    // Add article permissions to bt_delete_own_content role.
    if (in_array('user.role.bt_delete_own_content', $names)) {
      $faq_permissions = [
        'delete own faq content',
      ];
      $content_role = $this->deleteOwnContent;
      $permissions = array_merge($content_role->get('permissions'), $faq_permissions);
      $overrides['user.role.bt_delete_own_content']['permissions'] = $permissions;
    }
    // Add article permissions to bt_edit_publish_content role.
    if (in_array('user.role.bt_edit_publish_content', $names)) {
      $faq_permissions = [
        'edit any faq content',
      ];
      $content_role = $this->editContent;
      $permissions = array_merge($content_role->get('permissions'), $faq_permissions);
      $overrides['user.role.bt_edit_publish_content']['permissions'] = $permissions;
    }

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'ConfigFaqOverride';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
