<?php

namespace Drupal\bt_faq\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // Change title to FAQ page.
    if ($route = $collection->get('faq.faq-page')) {
      $route->setDefault('_title', 'Frequent Answer Question');
    }
    // Change title to FAQ Order page.
    if ($route = $collection->get('faq.faq-page.order')) {
      $route->setDefault('_title', 'Sort Questions');
    }
  }

}
